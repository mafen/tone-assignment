# Import necessary modules
import socket  # For socket operations
import json    # For JSON handling

# Define a function for the UDP client
def udp_client(host, port):

    try:
        while True:
            # Create and bind the socket to the specified host and port
            udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            udp_socket.bind((host, port))
            
            # Receive and decode the data
            data, server_address = udp_socket.recvfrom(1024)
            decoded_data = data.decode("utf-8")
            
            # Parse in to JSON
            weather_data = json.loads(decoded_data)
            
            # Print the JSON data
            print(json.dumps(weather_data))
    
    except socket.timeout:
        print("Connection to server timed out.")
    except KeyboardInterrupt:
        print("\nClient terminated by user.")
    except Exception as e:
        print(f"Error: {e}")
    finally:
        # Close the UDP socket when finished
        udp_socket.close()

# Entry point of the script
if __name__ == "__main__":
    # Define the UDP server's host and port
    udp_server_host = "0.0.0.0"   # Host IP address (0.0.0.0 means any available network interface)
    udp_server_port = 12345        # Port number for communication

    # Print connection information
    print(f"Connecting to UDP server at {udp_server_host}:{udp_server_port}")
    print("Press Ctrl+C to stop the client.")
    
    # Call the UDP client function with the specified host and port
    udp_client(udp_server_host, udp_server_port)
