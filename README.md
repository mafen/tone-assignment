# Assignment

Please sumbit solution as a link to public repository, or a zip file containing solution materials. It should contain explanation of the task solutions, docker file, client program source code and link to the codepen or similar.

# Weather data collection
This repository contains a program `main.py` that generates fake weather data and publishes it using UDP protocol on port 12345.

Task: Write a client that can read data published on localhost port 12345 using UDP protocol. The client should be able to translate the received data to NDJSON format and output the result to STDOUT. Use programming language of your choice and provide building and running instructions. Original program emulating the weather data stream should be packaged and ran as a containerized service. Suggest a schema for storing received semistructured data in a PostgreSQL database.

# Data illustration
[MOSJ.no](https://mosj.no) pulls all of its monitoring data as JSON from a data API.

Task: Use any open source graph library to recreate the graph for the [Ground temperature in permafrost, Janssonhaugen](https://mosj.no/en/indikator/climate/land/permafrost/) timeseries graph. Data for `15 m` depth is found at the following URL: [15 m data](https://api.npolar.no/indicator/timeseries/?facets=label.en&q=&filter-systems=mosj.no&filter-authors.@id=met.no&filter-keywords.@value=land&filter-locations.placename=Janssonhaugen&filter-label.en=15+m&format=json&variant=array&limit=1)

Substitute `15` with `25` and `40` in the `filter-label.en` parameter to get the data for the two other measured depths.

This exercise is a web programming exercise and should be completed using ECMAScript 2017 coding style with promises, fetch API, and async functions.
You should deliver and present your code using a codepen, glitch, jsfiddle, or similar web tool.