To start the server 

Set client address in the compose using env values 
CLIENT_ADDR=your-clients-ip address

Start once 
docker compose up --build 

Start in deamon mode
docker compose up -d

Start the client 

cd to client folder and python3 client.py