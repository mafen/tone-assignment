import random
import socket
import time
import hashlib
import json
import os

def generate_weather_data():
   temperature = round(random.uniform(10, 30), 2)
   humidity = round(random.uniform(30, 80), 2)
   pressure = round(random.uniform(980, 1050), 2)
   wind_speed = round(random.uniform(0, 20), 2)
   rainfall = round(random.uniform(0, 10), 2)

   weather_data = {
      "Temperature (deg C)": temperature,
      "Humidity (%)": humidity,
      "Pressure (hPa)": pressure,
      "Wind Speed (m/s)": wind_speed,
      "Rainfall (mm)": rainfall,
   }

   # Calculate checksum using MD5 hash
   data_json = json.dumps(weather_data, sort_keys=True)
   checksum = hashlib.md5(data_json.encode()).hexdigest()

   # Add the checksum to the weather data
   weather_data["Checksum"] = checksum

   return weather_data

def publish_weather_data_udp(host, port):
   udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

   while True:
      weather_data = generate_weather_data()
      data_json = json.dumps(weather_data)
      udp_socket.sendto(data_json.encode(), (host, port))
      print(f"Published: {data_json} to {host}:{port}")
      time.sleep(1)  # Adjust the sleep interval as needed

if __name__ == "__main__":
   # Define the UDP server's host and port
   udp_host = os.environ.get("CLIENT_ADDR", "127.0.0.1")
   udp_port = os.environ.get("CLIENT_PORT", 12345)

   print("Weather Station Simulation Started:")
   publish_weather_data_udp(udp_host, udp_port)